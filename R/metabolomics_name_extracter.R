#' seprate mz_rt based on names
#'@param string  A character string of metabolomics names, usually specified as RP smth mzrentention time 
#'@param mz_rt_separater the separater that separates mz and rt
#'@param name_mz_rt_separater the separater that seprates character and mz
#'@param old_name_tochange change the non-number non-separater part name
#'@param new_name the new name change to, from the non-number non-separater part name
#'@param letter_sepecial A special character that is used to identify clusters in the original name
#'@param which_must_have_digits If some stupid people name features as RP89.1154.447. Decide either retention time or mz has digits
#'@export
#'@return a resampled thing

## Biodiva
# string1<-c("HP_X38.213647543.181.67","RN_X40.2353264661463@333.435","RN_X40@333")
# # sMCC
# string2<-c("RNC040.44.3","RN61.988@42.67","RN99.9295.453.43","RN99.453")
# # MAX
# string3<-c("RPC12414@147.29","RN222.324.22.456","RNC222.45336")
# aa<-metabolomics_name_extracter(string1)
# bb<-metabolomics_name_extracter(string2)
# cc<-metabolomics_name_extracter(string3)

metabolomics_name_extracter<-function(
    string,
    mz_rt_separater,
    name_mz_rt_separater,
    old_name_tochange=NULL,
    new_name=NULL,
    letter_sepecial,
    which_must_have_digits=c("mz","rt")
){
  if(missing(which_must_have_digits)){
    which_must_have_digits<-"mz"
  }
    if(missing(letter_sepecial)){
      letter_sepecial<-"C"
    }
  if(missing(name_mz_rt_separater)){
    name_mz_rt_separater<-""
  }
  if(missing(mz_rt_separater)){
    mz_rt_separater<-"@"
  }
  string_character<-filterNonNumbersAndDotsat(string)
  
  if((!is.null(old_name_tochange))&(!is.null(new_name))){
    string_character_new<-change_name(string_character,
                                      old_name_tochange,
                                      new_name)
  }else{
    string_character_new<-string_character
  }
  string_non_character<-extractNumbersAndDotsat(string)
  
  string_non_character_non_number_list<-findNonNumbers(string_non_character)
  string_non_character_number_list<-findNumbers(string_non_character)
  numberfront<-vector(length=length(string_non_character_non_number_list))
  numberback<-vector(length=length(string_non_character_non_number_list))
  clustered_metabolomics_position<-c()
  non_clustered_metabolomics_position<-c()
  
  letter_special<-letterInString(string_character,"C")
  
  for(i in 1:length(string)){
    
    if(letter_special[i]){
      clustered_metabolomics_position<-c(clustered_metabolomics_position,i)
    }else{
      
      non_clustered_metabolomics_position<-c(non_clustered_metabolomics_position,i)
    }
  }
  
  for (i in 1:length( string_non_character_non_number_list)){
    if(length(string_non_character_non_number_list[[i]])==2){
      if(which_must_have_digits=="rt"|letter_special[i]){
        numberfront[i]<-string_non_character_number_list[[i]][1]
        numberback[i]<-combineNumbers(string_non_character_number_list[[i]][2],   ## default to believe retention time has digits
                                      string_non_character_number_list[[i]][3])
      }else if(which_must_have_digits=="mz"){
        numberfront[i]<-combineNumbers(string_non_character_number_list[[i]][1],   ## default to believe retention time has digits
                                       string_non_character_number_list[[i]][2])
        numberback[i]<-string_non_character_number_list[[i]][3]
      }
      #clustered_metabolomics_position<-c(clustered_metabolomics_position,i)
    }else if (length(string_non_character_non_number_list[[i]])==3){
      numberfront[i]<-combineNumbers(string_non_character_number_list[[i]][1],
                                     string_non_character_number_list[[i]][2])
      numberback[i]<-combineNumbers(string_non_character_number_list[[i]][3],
                                    string_non_character_number_list[[i]][4])
      #non_clustered_metabolomics_position<-c(non_clustered_metabolomics_position,i)
    }else if (length(string_non_character_non_number_list[[i]])==1){
      numberfront[i]<-string_non_character_number_list[[i]][1]
      numberback[i]<-string_non_character_number_list[[i]][2]
      #non_clustered_metabolomics_position<-c(non_clustered_metabolomics_position,i)
    }
  }
  
  string_non_character_new<-vector(length=length(string))
  string_new<-vector(length=length(string))
  
  for(i in 1:length(string)){
    string_non_character_new[i]<-str_paste0(c(numberfront[i],
                                              mz_rt_separater,
                                              numberback[i]))
    string_new[i]<-str_paste0(c(string_character_new[i],
                                name_mz_rt_separater,
                                string_non_character_new[i]))
  }
  
  # numberfront<-as.numeric(numberfront)
  numberfront<-as.character(numberfront)
  numberback<-as.character(numberback)
  #numberback<-as.numeric(numberback)
  result<-list()
  result$info<-cbind.data.frame(
    old_name=string,
    mode_name_old=string_character,
    mode_name_new=string_character_new,
    mz_or_cluster=numberfront,
    rt=numberback,
    mz_rt_old=string_non_character,
    mz_rt_new=string_non_character_new,
    new_name=string_new
  )
  if(length(clustered_metabolomics_position)!=0){
    result$clustered_metabolomics<-result$info[clustered_metabolomics_position,]
    
  }
  if(length(non_clustered_metabolomics_position)!=0){
    result$non_clustered_metabolomics<-result$info[non_clustered_metabolomics_position,]
  }
  if((length(clustered_metabolomics_position)!=0)&(length(clustered_metabolomics_position)!=0)){
    result$info_reordered<-rbind(result$non_clustered_metabolomics,
                                 result$clustered_metabolomics)
  }
  result
  return(result)    
}

#' @export
filterNonNumbersAndDotsat <- function(vector) {
  # Use gsub to replace all numbers and dots with an empty string
  result <- gsub("[0-9.@]", "", vector)
  
  return(result)
}

#' @export
extractNumbersAndDotsat <- function(vector) {
  # Use gsub to replace everything except numbers and dots with an empty string
  result <- gsub("[^0-9.@]", "", vector)
  
  return(result)
}


#' @export
findNonNumbers <- function(vector) {
  # Use sapply to apply the function to each element of the vector
  result <- sapply(vector, function(element) {
    # Use gsub to replace numbers with an empty string
    non_numbers <- gsub("[0-9]", "", element)
    # Convert the result to a list
    as.list(strsplit(non_numbers, NULL))
  })
  
  return(result)
}





#' @export
findNumbers <- function(vector) {
  library(stringr)
  # Use str_extract_all to extract all numbers from each element
  result <- str_extract_all(vector, "\\d+")
  
  return(result)
}


#' @export
combineNumbers <- function(number1, number2) {
  combined <- paste0(number1, ".", number2)
  return(combined)
}

#' @export
letterInString <- function(vector, letter) {
  # Use grepl to check if the letter is present in each element
  result <- grepl(letter, vector)
  
  return(result)
}

#' @export
extract_numbers_from_vector <- function(string_vector) {
  # Function to extract numbers from a single string
  extract_from_string <- function(string) {
    numbers <- regmatches(string, gregexpr("\\d+(?:\\.\\d+)?", string, perl = TRUE))
    as.character(unlist(numbers))
  }
  
  # Apply the extraction function to each string in the vector
  result <- lapply(string_vector, extract_from_string)
  
  # Return the result as a list
  return(result)
}

#' @export
extract_numbers_one_separater <- function(vector) {
  # Use regular expression to extract numbers
  pattern <- "^(\\d+\\.\\d+)_(\\d+\\.\\d+)$"
  
  # Extract matches
  matches <- regexec(pattern, vector)
  extracted <- regmatches(vector, matches)
  
  # Prepare result dataframe
  result <- data.frame(
    original = vector,
    before_underscore = numeric(length(vector)),
    after_underscore = numeric(length(vector))
  )
  
  # Fill in extracted values
  for (i in seq_along(extracted)) {
    if (length(extracted[[i]]) == 3) {
      result$before_underscore[i] <- as.numeric(extracted[[i]][2])
      result$after_underscore[i] <- as.numeric(extracted[[i]][3])
    }
  }
  
  return(result)
}
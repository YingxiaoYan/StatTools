#' Make a color bar 
#' @param value1 Character with OTU taxonomic information (e.g. "D_0__Bacteria;D_1__Firmicutes;D_2__Clostridia;D_3__Clostridiales;D_4__Lachnospiraceae;D_5__Blautia" ...)
#' @param value2 Splitting character between taxonomic levels (e.g. ";" which is default)
#' @param sig1 significance value of 
#' @param sig2 
#' @param sig_cutoff cut off of significance value
#'
#' @return A ggplot object
#' @export
#'
#' 
two_direction_plot<-function(value1,
                             value2,
                             sig1,
                             sig2,
                             sig_cutoff=0.05){
  library(ggplot2)
  lim<-max(abs(value1),abs(value2))
  if(value1>=0){
    color1<-"red"
    }else{
    color1<-"blue"
    }
  if(value2>=0){
    color2<-"red"
  }else{
    color2<-"blue"
  }
  if(sig1>sig_cutoff){color1<-"grey"}
  if(sig2>sig_cutoff){color2<-"grey"}
  
  ##If positive red color,negative blue, not significance
  p<-ggplot()+
   # geom_vline(xintercept = 0,
  #             linetype=2) +
    geom_hline(yintercept = 0,
               linetype=2)  +
    annotate("rect", 
             xmin=ifelse(value1>=0,0,value1), 
             xmax=ifelse(value1>=0,value1,0), 
             ymin=0, 
             ymax=lim/10, 
             alpha=1, fill=color1)+
    annotate('rect', 
             xmin=ifelse(value2>=0,0,value2), 
             xmax=ifelse(value2>=0,value2,0), 
             ymin=-lim/10, 
             ymax=0, 
             alpha=1, fill=color2)+
    annotate('segment', 
             x=0, 
             xend=0, 
             y=-lim/10, 
             yend=lim/10,
             lwd=1)+
    annotate('segment', 
             x=0, 
             xend=value1, 
             y=0, 
             yend=0,
             lwd=1)+
    annotate('segment', 
             x=0, 
             xend=value2, 
             y=0, 
             yend=0,
             lwd=1)+
    annotate('segment', 
             x=value1, 
             xend=value1, 
             y=0, 
             yend=lim/10,
             lwd=1)+
    annotate('segment', 
             x=value2, 
             xend=value2, 
             y=0, 
             yend=-lim/10,
             lwd=1)+
    annotate('segment', 
             x=0, 
             xend=value1, 
             y=lim/10, 
             yend=lim/10,
             lwd=1)+
    annotate('segment', 
             x=0, 
             xend=value2, 
             y=-lim/10, 
             yend=-lim/10,
             lwd=1)+
  annotate('text', 
           x=value1/2,
           y=lim/10/2,
           label=paste("DE=",value1),
           cex=6)+
    annotate('text', 
             x=value2/2,
             y=-lim/10/2,
             label=paste("IE=",value2),
             cex=6)+
    #xlim(-2,2) + ylim(-2,2)+
    scale_y_continuous(limits=c(-lim/10*1.1,lim/10*1.1))+
    scale_x_continuous(limits=c(-lim,lim))+
    ylab(" ")+
    xlab("Direct (top) and Indirect (bottom) effect estimate")+
   # theme_minimal()
    theme(
      # Remove panel border
      panel.border = element_blank(),
      # Remove panel grid lines
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      # Remove panel background
      panel.background = element_blank(),
      # Add axis line
      axis.line.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.y=element_blank()
    )

  shift_axis_y <- function(p, y=0){
    g <- ggplotGrob(p)
    dummy <- data.frame(y=y)
    ax <- g[["grobs"]][g$layout$name == "axis-b"][[1]]
    p + annotation_custom(grid::grobTree(ax, vp = grid::viewport(y=1, height=sum(ax$height))), 
                          ymax=y, ymin=y) +
      geom_hline(aes(yintercept=y), data = dummy,lty=2) +
      theme(axis.text.x = element_blank(), 
            axis.ticks.x=element_blank(),
            # Add axis line
            axis.line.y = element_blank(),
            axis.text.y=element_blank(),
            axis.ticks.y=element_blank(),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            # Remove panel background
            panel.background = element_blank())
  }
  p<-shift_axis_y(p)
return(p)
    
  
}


